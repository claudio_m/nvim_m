local status_ok, venv = pcall(require, "venv-selector")
if not status_ok then
  return
end

venv.setup({
    activate_venv_in_terminal = true,
    enable_cached_venvs = true,
    cached_venv_automatic_activation = true,

     -- telescope viewer options
    on_telescope_result_callback = nil,
    show_telescope_search_type = true,
    telescope_filter_type = "substring"
})

