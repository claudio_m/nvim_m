local status_ok, eslint = pcall(require, "nvim-eslint")
if not status_ok then
  print('error eslint')
  return
end

eslint.setup({})
