--[[ local on_attach = function(_, bufnr) ]]
--[[   -- Enable completion triggered by <c-x><c-o> ]]
--[[   vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc') ]]
--[[ end ]]

return {
	settings = {
        go = {
            analyses = {
                unusedparams = true,
            },
            staticcheck = true,
            gofumpt = true,
            --[[ on_attach = on_attach, ]]
            cmd = {"gopls"},
            filetypes = { "go", "gomod", "gowork", "gotmpl" },
            single_file_support = true,
            codelenses = {
                gc_details = false,
                generate = true,
                regenerate_cgo = true,
                run_govulncheck = true,
                test = true,
                tidy = true,
                upgrade_dependency = true,
                vendor = true,
            },
            hints = {
                assignVariableTypes = true,
                compositeLiteralFields = true,
                compositeLiteralTypes = true,
                constantValues = true,
                functionTypeParameters = true,
                parameterNames = true,
                rangeVariableTypes = true,
            },
            --[[ analyses = { ]]
            --[[     fieldalignment = true, ]]
            --[[     nilness = true, ]]
            --[[     unusedparams = true, ]]
            --[[     unusedwrite = true, ]]
            --[[     useany = true, ]]
            --[[ }, ]]
            usePlaceholders = true,
            completeUnimported = true,
            --[[ staticcheck = true, ]]
            directoryFilters = { "-.git", "-.vscode", "-.idea", "-.vscode-test", "-node_modules", "-.nvim" },
            semanticTokens = true,


            --[[ settings = { ]]
            --[[     gopls = { ]]
            --[[         completeUnimported = true, ]]
            --[[         usePlaceholders = true, ]]
            --[[         analyses = { ]]
            --[[             unusedparams = true, ]]
            --[[         }, ]]
            --[[     }, ]]
            --[[ }, ]]
        },
    }
}

