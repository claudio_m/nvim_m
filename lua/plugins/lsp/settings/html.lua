local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

return {
	settings = {
        html = {
            cmd = {"vscode-html-language-server", "--stdio"},
            filetypes = {"html", "templ", "htmldjango"},
            capabilities = capabilities,
            init_options = {
                configurationSection = { "html", "css", "javascript" },
                embeddedLanguages = {
                    css = true,
                    javascript = true
                },
                provideFormatter = true
            }
        }
	},
}

