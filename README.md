# nvim_m
My neovim config

## Instalation

### Dependencies

Package `fd` for venv-select
```
https://github.com/sharkdp/fd
```

`ripgrep` for telescope greep...
```
https://github.com/BurntSushi/ripgrep#installation
```

### Linux
```
git clone https://gitlab.com/claudio_m/nvim_m.git ~/.config/nvim
```

### Windows
- PowerShell
```
git clone https://gitlab.com/claudio_m/nvim_m.git "$Env:LOCALAPPDATA\nvim" 
```
- CMD
```
git clone https://gitlab.com/claudio_m/nvim_m.git %USERPROFILE%\AppData\Local\nvim  
```

Run `nvim` and wait for the plugins to be installed

